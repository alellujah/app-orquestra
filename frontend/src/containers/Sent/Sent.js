import React, { Component } from 'react';
import axios from 'axios';
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css'; // This only needs to be imported once in your app


export class Sent extends Component{
  constructor(props){
    super(props)
    this.state = {
      data: null,
      img: null,
      isOpen: false
    }               
  }
  getPosts = () => {
    axios.get('http://localhost:4000/enviarhoras')
    .then((result) => {
      let data = JSON.parse(result.data.userData);
      this.setState({
        data: data        
      });      
    });  
  }
  componentDidMount(){
    this.getPosts();
  }
  openLightBox(url){    
    this.setState({
      img: url,
      isOpen: true
    });
  }
  
  render(){
    const {data,isOpen,img} = this.state;
    return(
      <div>
        <h1>Horas Enviadas</h1>                        
        {data != null && 
          data.map((element, i) => {
            let url = "http://localhost:4000" + element[0].substring(1);
            return(
              <div className="card" key={i}>     
                <a onClick={() => this.openLightBox(url)}>
                  <img alt={element[1]} src={url} />                
                </a>                                           
                <span><strong>Enviado a:</strong> {element[1]}</span>
              </div>
            )            
          })
        }
        {isOpen &&        
        <Lightbox mainSrc={img} onCloseRequest={() => this.setState({ isOpen: false })} />
        }
      </div>
    )
  }
}
