import React, { Component } from 'react';
import { Link } from 'react-router';
import { Icon } from 'semantic-ui-react';

export class Navigation extends Component{    
    render(){
        return(
            <ul>
                <li>
                    <Link href="/">                     
                        <span className="icon"><Icon name="upload" /></span>                                               
                        <span className="text">Enviar</span>
                    </Link>
                </li>
                <li>
                    <Link href="/horas"> 
                    <span className="icon"><Icon name="history" /></span>                                               
                    <span className="text">Enviadas</span>  
                    </Link>
                </li>
                {/* //TODO */}
                {/* <li>
                    <Link href="/pagamentos"> Pagamentos </Link>
                </li> */}
                <li>                
                    <Link href="/configuracoes"> 
                    <span className="icon"><Icon name="configure" /></span>                                               
                    <span className="text">Configs</span>  
                    </Link>
                </li>
            </ul>
        )
    }
}