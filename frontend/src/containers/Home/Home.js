import React, { Component } from 'react';
import axios from 'axios';
import { Input, Form, Button } from 'semantic-ui-react';
import { Icon } from 'semantic-ui-react';

export class Home extends Component{
  constructor(props){
    super(props)
    this.state = {
      userName: 'Paula',
      message: 'Boa noite, envio em anexo as minhas horas.',
      data_uri: null,
      sent: false,
      loading: false
    };
    this.parseImage = this.parseImage.bind(this)
  }
  onChange = (e) => {
    // Because we named the inputs to match their corresponding values in state, it's
    // super easy to update the state
    const state = this.state
    state[e.target.name] = e.target.value;
    this.setState(state);
  }
  onSubmit = (e) => {
    e.preventDefault();
    // get our form data out of state
    const { userName, message, data_uri } = this.state;
    if (data_uri == null){
      alert('Insira uma imagem!')
    } 
    else{
      this.setState({
        loading: true
      })
      axios.post('http://localhost:4000/enviarhoras', { userName, message, data_uri })      
      .then((result) => {
        console.log(result)
        this.setState({
          sent: true,
          data_uri: null,
          loading: false
        });
      });
    }    
  }
  parseImage = (e) => {
    const reader = new FileReader();
    const file = e.target.files[0];

    reader.onload = (upload) => {
      this.setState({
        data_uri: upload.target.result,        
      });
    };
    reader.readAsDataURL(file);    
  }

  render(){
    const { userName, message, data_uri, sent } = this.state;    
    return(
      <div>        
        <Form onSubmit={this.onSubmit}>          
          {sent !== true ?          
          <div>
            <h1> Recibos Verdes</h1>        
            <div className="equal width fields">
              <Form.Field>              
                <label>Nome</label>
                <Input type="text" name="userName" value={userName} placeholder={userName} onChange={this.onChange} />
              </Form.Field>
              <Form.Field>
                <label>Mensagem</label>
                <Input type="text" name="message" value={message} placeholder={message} onChange={this.onChange} />
              </Form.Field>            
            </div>            
            <Form.Group widths="equal">
              <Form.Field>
                  {/* <label>Captura de imagem</label> */}
                  <div className="upload-btn-wrapper">
                    <button className="btn">{data_uri != null ? <span><Icon name="check" /> Upload feito</span> : <span><Icon name="camera" /> Tirar ou carregar fotografia</span>}</button>
                    <input type="file" accept="image/*" capture="camera" name="image" onChange={this.parseImage} />
                  </div>
                  {/* <Input type="file" accept="image/*" capture="camera" name="image" onChange={this.parseImage} /> */}
              </Form.Field>
            </Form.Group>   

            <Button fluid primary name="submit" loading={this.state.loading} type="submit">Enviar horas</Button>
            </div>
            :
            <div className="sent">
              <h1 style={{textAlign: 'center', fontSize:'55px'}}>E-mail enviado!</h1>
            </div>
          }
        </Form>
        <br/>
      </div>
    )
  }
}
