import React, { Component } from 'react';
import axios from 'axios';
import { Input, Form, Button } from 'semantic-ui-react';

export class Login extends Component{
  constructor(props){
    super(props)
    this.state = {
      userName: "Utilizador",
      password: "Password",
      loading: false,
      token: null
    };    
  }
  onChange = (e) => {
    // Because we named the inputs to match their corresponding values in state, it's
    // super easy to update the state
    const state = this.state
    state[e.target.name] = e.target.value;
    this.setState(state);
  }
  onSubmit = (e) => {
    e.preventDefault();
    // get our form data out of state
    const { userName, password } = this.state;
    if ((userName || password) == null){
      alert('Preencha os campos!')
    } 
    else{
      this.setState({
        loading: true
      })
      axios.post('http://localhost:4000/login', { userName, password })      
      .then((result) => {
        console.log(result)
        this.setState({
          sent: true,
          userName: result[0],
          token: result[1],
          loading: false
        });
        console.log(this.state.userName);
        sessionStorage.setItem('testing', result[0]);
      });
    }    
  }

  render(){
    const { userName, password, sent } = this.state;    
    return(
      <div>        
        <Form onSubmit={this.onSubmit}>          
          {sent !== true ?          
          <div>
            <h1> Login</h1>        
            <div className="equal width fields">
              <Form.Field>              
                <label>Nome</label>
                <Input type="text" name="userName" value={userName} placeholder={userName} onChange={this.onChange} />
              </Form.Field>
              <Form.Field>
                <label>Password</label>
                <Input type="password" name="password" value={password} placeholder={password} onChange={this.onChange} />
              </Form.Field>            
            </div>            
            
            <Button fluid primary name="submit" loading={this.state.loading} type="submit">Entrar</Button>
            </div>
            :
            <div className="sent">
              <h1 style={{textAlign: 'center', fontSize:'55px'}}>Login feito!</h1>
            </div>
          }
        </Form>
        <br/>
      </div>
    )
  }
}
