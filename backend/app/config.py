class Config(object):
    DEBUG = False
    TESTING = False
    MYSQL_DATABASE_HOST = ''
    MYSQL_DATABASE_DB = ''
    MYSQL_DATABASE_USER = ''
    MYSQL_DATABASE_PASSWORD = ''
    MAIL_SERVER = ''
    MAIL_PORT = ''
    MAIL_USERNAME = ''
    MAIL_PASSWORD = ''

class ProductionConfig(Config):
    pass #empty for now

class DevelopmentConfig(Config):
    DEBUG = True
    TESTING = True
    MYSQL_DATABASE_HOST = 'database'
    MYSQL_DATABASE_DB = 'flask_db'
    MYSQL_DATABASE_USER = 'flask'
    MYSQL_DATABASE_PASSWORD = 'flask'
    MAIL_SERVER = 'smtp.mailtrap.io'
    MAIL_PORT = '2525'
    MAIL_USERNAME = 'a3ca079e47969e'
    MAIL_PASSWORD = 'f7f93e7006e49f'

config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig,
}
