#flask packages
from flask import Flask
from flask_restful import Resource, Api, reqparse
from flask_cors import CORS, cross_origin # to solve cors on localhost
from flaskext.mysql import MySQL
from flask_mail import Mail, Message
#Python packages
import os
import base64
import unidecode
from time import gmtime, strftime
import json


#init vars
app = Flask(__name__, static_folder='uploads', static_url_path='/uploads')
api = Api(app)
CORS(app) # to solve cors on localhost
mysql = MySQL()
mail = Mail()

#Dev Config
# app.config.from_object('config.DevelopmentConfig') #not working with the mail..

#mail
app.config['MAIL_SERVER'] = 'smtp.mailtrap.io'
app.config['MAIL_PORT'] = '25'
app.config['MAIL_USERNAME'] = 'a3ca079e47969e'
app.config['MAIL_PASSWORD'] = 'f7f93e7006e49f'
#db
app.config['MYSQL_DATABASE_HOST'] = 'database'  #database is the name of the container!
app.config['MYSQL_DATABASE_DB'] = 'flask_db'
app.config['MYSQL_DATABASE_USER'] = 'flask'
app.config['MYSQL_DATABASE_PASSWORD'] = 'flask'

#init app
mysql.init_app(app)
mail.init_app(app)
#connect to db
conn = mysql.connect()
cursor = conn.cursor()

#Functions
def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print ('Error: Creating directory. ' +  directory)

# Flask Api
@app.route("/")
def show_entries():
    return("Access Denied")

class Login(Resource):
    def get(self):
        data = {[["Paula"],["Tokeeeen"]]}
        return data
    def post(self):
        pass

class Hours(Resource):
    def get(self):
        select_stmt =  "SELECT image,timesent FROM tabela_teste ORDER BY timesent DESC"
        cursor.execute(select_stmt)
        data = cursor.fetchall()
        return {'userData': json.dumps(data,indent=4, sort_keys=True, default=str)}
        cursor.close()
        conn.close()

    def post(self):
        try:
            # Parse the arguments
            parser = reqparse.RequestParser()
            parser.add_argument('userName', type=str, help='Nome do utilizador')
            parser.add_argument('message', type=str, help='Mensagem (opcional)')
            parser.add_argument('data_uri', type=str, help='Foto das horas')
            args = parser.parse_args()
            # return(app.config.from_object('config.DevelopmentConfig'))
            # From the frontend
            _userName = args['userName']
            _message = args['message']
            _data_uri = args['data_uri']
            # Added by the backend
            _time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
            _state = 'Enviadas Horas'

            # Saving the image and creating folder structure
            folder = './uploads/' + unidecode.unidecode(_userName.lower()) + '/' + strftime("%Y") + '/' + strftime("%m") + "/"
            imageUpload = folder + unidecode.unidecode(_userName.lower()) + ".jpg"
            createFolder(folder)
            imageToSave = _data_uri.split(',')[1]
            with open(imageUpload, "wb") as fh:
                fh.write(base64.b64decode(imageToSave))

            #Insert into the DB
            insert_stmt =  "INSERT INTO tabela_teste (name, message, image, timesent, state) VALUES (%s, %s, %s, %s, %s)"
            data = (_userName,_message,imageUpload,_time,_state)
            cursor.execute(insert_stmt, data)
            conn.commit()

            #Sending email
            msg = Message(_message + " - " + _userName,
            sender= (_userName, "noreply@plataformaog.com"),
            recipients=["teste@teste.com"])
            msg.attach('Horas ' + unidecode.unidecode(_userName) + ' - ' + strftime("%m/%Y", gmtime()) + ".jpg", "image/jpg", base64.b64decode(imageToSave))
            msg.html = "<p>Viva,</p> <p>Seguem em anexo as horas referentes ao mês atual.</p> <p>Cumprimentos, " + _userName + "</p>"
            mail.send(msg)

            #Return 200 if all is OK!
            return {'StatusCode':'200','Message': 'Enviado com sucesso'}

        except Exception as e:
            #Return error
            return {'error': str(e)}

#add url to post
api.add_resource(Login, '/login')
api.add_resource(Hours, '/enviarhoras')

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, port=4000)
