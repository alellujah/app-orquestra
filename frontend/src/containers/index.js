import { Home } from './Home/Home';
import { Sent } from './Sent/Sent';
import { Payments } from './Payments/Payments';
import { Configs } from './Configs/Configs';
import { NotFound } from './NotFound/NotFound'
import { Login } from './Login/Login';

export {
  Home,
  Sent,
  Payments,
  Configs,
  NotFound,
  Login
}
