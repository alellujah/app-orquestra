# App Orquestra
## PWA para enviar horas e emitir recibos verdes.

**Todo**

1. Aceder à câmera do telemóvel para tirar foto da folha de horas
2. Enviar e-mail com foto e titulo informativo, ex: "Horas Paula - Novembro"
3. Usar um headless browser para submeter recibos verdes (PhantomJS p exemplo)
4. Guardar recibo
5. Enviar recibo

### Instruções
1. Instalar Docker
2. Na primeira vez fazer **docker-compose build**
3. Depois fazer **docker-compose up** para iniciar frontend e backend, caso queira só iniciar um, docker-compose up api ou docker-compose frontend


**Utilities**
https://github.com/wsargent/docker-cheat-sheet
