import React, { Component } from 'react';
import { Router, browserHistory, Route } from 'react-router';
import { Home, Sent, Payments, Configs, NotFound, Login } from './containers';
import { Navigation } from './components';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (      
      <div className="ui grid">
        <div className="main">            
          <div className="sixteen wide column">
            <Router history={browserHistory}>
              <Route path="/login" component={Login}/>
              <Route path="/" component={Home}/>
              <Route path="/horas" component={Sent}/>
              <Route path="/pagamentos" component={Payments}/>
              <Route path="/configuracoes" component={Configs}/>
              <Route path="*" component={NotFound}/>
            </Router>
          </div>
      </div>
      <nav>
        <Navigation />
      </nav>
    </div>
    );
  }
}

export default App;
